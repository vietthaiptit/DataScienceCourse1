def sum(x, y):
    sum = x + y
    if sum in range(15, 20):
        return 20
    else:
        return sum

print(sum(10, 6)) # Output : 20 | sum = 16 |  15 < sum < 20 return 20
print(sum(10, 2)) # Output : 20 | sum = 16 |  sum < 15  return sum
print(sum(10, 12)) # Output : 20 | sum = 16 |  sum > 20  return sum