import datetime

now = datetime.datetime.now()
print("Current date and time : ")
print(now.strftime("%Y-%m-%d %H:%M:%S"))

# Output will be the current date and time of system which is printed with format YYYY-MM-DD hh:mm:ss
# %Y - year including the century
# %m - month (01-12)
# %d - day of the month (01 to 31)
# %H - hour, using a 24-hour clock (00 to 23)
# %M - minute (00-59)
# %S - second (00-59)
# Example : 2018-07-16 00:49:08