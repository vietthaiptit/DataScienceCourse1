# Exercise 4:
# Write a python script to randomly generate a list of N integers on interval [0,10]
# (N is a number given by user) and then:
#○	Create and print a histogram of the list (i.e., count frequency of occurrences).
#○	Manually calculate and print the statistics description of the list
# (i.e, max, min, mean, variance and standard deviation).
# You may need to import math library and use function sqrt for this calculation.
#○	Remove duplicates from the list


import random as rd
import math

n = input("Enter N = ")
listNumber = []
for x in range(int(n)):
  listNumber.append(rd.randint(0,10))
listNumber.sort()
print(listNumber)

for i in range(0,11):
  time = listNumber.count(i)
  if time > 0:
    if i < 10:
      print('{k}  {a}'.format(k=i, a='*' * int(time)))
    else:
      print('{k} {a}'.format(k=i, a='*' * int(time)))

#max
print("Max = ",max(listNumber))

#min
print("Min = ",min(listNumber))

#mean
mean = sum(listNumber)/len(listNumber)
print("Mean = ",mean)

#variance
sum = 0.0
n = len(listNumber)
for number in listNumber:
  sum += (mean - number) ** 2
variance = sum / (n - 1)
print("Variance = ",variance)

# standard deviation
standardDeviation=math.sqrt(variance)
print("Standard Deviation = ",standardDeviation)

## Remove duplciate lists
# A set is guaranteed to not have duplicates.
print(list(set(listNumber)))

