# 6.	Write a python script to get sum of column of matrix N x M (i.e, list in a list). For example:
# M = [[1,2,3,4,5],
#      [3,4,2,5,6],
#      [1,6,3,2,5]]
# OUTPUT: [5, 12, 8, 11, 16]
import random as rd
def generate_matrix(n, m):
    list = []
    for i in range(0,n):
        tmpList = []
        for j in range(0,m):
            tmpList.append(rd.randint(0, 10))
        list.append(tmpList)
        print(tmpList)
    return list

def sum_column_matrix(matrixList):
    resultList = []
    n = len(matrixList[0])
    for i in range(0,n):
        sum = 0
        for list in matrixList:
            sum +=list[i]
        resultList.append(sum)
    return resultList

n = input("Enter n = ")
m = input("Enter m = ")
matrixList = generate_matrix(int(n),int(m))
print(sum_column_matrix(matrixList))

