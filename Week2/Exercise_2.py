# Exercise 2.
# Given the following list l=[(1,4,'ewe','5'), ('21', 0.4, 4, [31,3,5]), [7,3,'s',2], [4,2,6,'dad'], {3,5}]],
# sort this list by sum of its item which has type is int or float (no recursive).
# OUTPUT:  [('21', 0.4, 4, [31, 3, 5]),  (1, 4, 'ewe', '5'),  {3, 5},  [7, 3, 's', 2],  [4, 2, 6, 'dad']]
# GUIDE: check type(x) is int or float
l = [(1, 4, 'ewe', '5'), ('21', 0.4, 4, [31, 3, 5]), [7, 3, 's', 2], [4, 2, 6, 'dad'], {3, 5}]

def sum_list(list):
    if len(list) == 0:
        return 0
    sum = 0
    for item in list:
        if (type(item) is int) or (type(item) is float):
            sum += item
    return sum

l.sort(key=sum_list)
print(l)
