# Exercise 5
# Write a python script to generate a list of 100 numbers of float
# using the function y = x ^ 2 / (4*x), x on interval (0, 10] and linear space.

listNumber = []
for i in range(1,101):
    x = 0.1 * i
    y = (x ** 2) / (4*x)
    listNumber.append(round(y,5))
print(listNumber)