# Exercise 1
# Write a python script that gets a string of number
# from user, e.g., 12, 32, 31, 12, 42, 21, 58, 92,37,45
# Using this string to generate 2 list of numbers,
# the first list consists of even numbers
# and the second one is the list of odd numbers.

inputStr = input("Input : ")
numberList = inputStr.split(",")
print (numberList)
evenNumberList = []
oddNumberList = []
for numberStr in numberList:
    number = int(numberStr.strip())
    if number % 2 == 0:
        evenNumberList.append(number)
    else:
        oddNumberList.append(number)
print(evenNumberList)
print(oddNumberList)