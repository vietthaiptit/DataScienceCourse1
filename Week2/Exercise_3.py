# Exercise 3.	Given a string s = 'the output that tells you if the object is the type you think or not',
# ○	sort the words of this string by alphabet.
# OUTPUT: 'if is not object or output tells that the the the think type you you'
# ○	upper case all the first character of the words
# OUTPUT: 'The Output That Tells You If The Object Is The Type You Think Or Not'
# GUIDE: both use split and join of string

s = 'the output that tells you if the object is the type you think or not'
space=" "

strList1 = s.split(" ")
strList1.sort()
print(space.join(strList1))

strList2 = s.split(" ")
for (i, word) in enumerate(strList2):
    strList2[i] = word.capitalize()
print(space.join(strList2))