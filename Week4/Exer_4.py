import numpy as np
import pandas as pd

ids=['id1','id2','id3','id4','id5','id6','id7','id8','id9','id10','id11','id12','id13','id14','id15','id16','id17','id18','id19','id20']
rangeList = range(-100,100)
s1=pd.Series(np.random.choice(ids, 10, False))
s2=pd.Series(np.random.choice(ids, 10, False))
s3=pd.Series(np.random.choice(rangeList, 10, False))
s4=pd.Series(np.random.choice(rangeList, 10, False))
df1=pd.DataFrame({'key' : s3,'data1':s1})
df2=pd.DataFrame({'key' : s4,'data2':s2})
dfinal=df1.merge(df2,on="key",how = 'inner')
print(df1)
print(df2)
print(dfinal)