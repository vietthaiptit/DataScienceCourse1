#2.	Write a python script to:
#○	Load the iris file to a DataFrame of Pandas (https://archive.ics.uci.edu/ml/machine-learning-databases/iris/ )
#○	Calculate the statistical description of sepal_length, sepal_width, petal_length and petal_width by type of flower.
## (Use groupby to solve your problem:https://pandas.pydata.org/pandas-docs/stable/groupby.html )
#○	For each type of flower, normalize its sepal_length and petal_length
## by replacing the outer line 50% distance to mean (i.e., x = (x + mean)/2 ). You can choose a distance to define outer line.

import pandas as pd

def normalize(df):
    result = df.copy()
    for feature_name in df.columns:
        mean_value_1 = df['sepal_length'].mean()
        mean_value_2 = df['petal_length'].mean()
        result['sepal_length'] = (df['sepal_length'] - mean_value_1) / 2
        result['petal_length'] = (df['petal_length'] - mean_value_2) / 2
    return result

df = pd.read_csv("iris.txt")
#print(df)
grouped = df.groupby('class')
grouped_1=grouped.get_group('Iris-setosa')
print(grouped_1)
print(grouped_1.describe())
normalize_grouped_1=normalize(grouped_1)
print(normalize_grouped_1)

grouped_2=grouped.get_group('Iris-versicolor')
print(grouped_2)
print(grouped_2.describe())
normalize_grouped_2=normalize(grouped_2)
print(normalize_grouped_2)

grouped_3=grouped.get_group('Iris-virginica')
print(grouped_3)
print(grouped_3.describe())
normalize_grouped_3=normalize(grouped_3)
print(normalize_grouped_3)


