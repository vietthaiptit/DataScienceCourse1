#3.	Write a python script to:
#○	Generate a DataFrame where the first column is a range of date in month
#(use date_range with freq = ‘M’ and periods = 50, starting from 2018-01-31
#to generate values for this column https://pandas.pydata.org/pandas-docs/stable/generated/pandas.date_range.html)
#and the second one is a correspondent random number (called ‘sale’) in range(10,30).
#○	Calculate the sum of sale by year.  (Guide: add a new column ‘year’ from the date and use groupby https://pandas.pydata.org/pandas-docs/stable/groupby.html).
#○	OPTIONAL: You can change freq=’D’ and periods=200 then calculate the sum of sale by month.
import pandas as pd
import numpy as np

date= pd.date_range('2018-01-31', periods=50, freq='M')
sale=np.random.randint(10,30,size=50)
df=pd.DataFrame({'Date': date, 'Sale': sale})
df['Year']=pd.DatetimeIndex(df['Date']).year
print(df.groupby(['Year'])['Sale'].sum())

# Optional
date1= pd.date_range('2018-01-31', periods=200, freq='D')
sale1=np.random.randint(10,30,size=200)
df1=pd.DataFrame({'Date': date1, 'Sale': sale1})
df1['Month']=pd.DatetimeIndex(df1['Date']).month
print(df1.groupby(['Month'])['Sale'].sum())