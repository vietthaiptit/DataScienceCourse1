#1.	Write a python script to:
#○	Given the following string “Ah meta descriptions… the last bastion of traditional marketing! The only cross-over point between marketing and search engine optimisation! The knife edge between beautiful branding and an online suicide note!”, split it into list of words and then use this list to create a Series of word.
#○	Filter to extract all words that contains at least 3 vowels (i.e., ‘a’,’i’,’o’,’e’,’u’) of the Series.
#GUIDE:
#➔	Use count function of string to count vowels in a string.
#➔	use map and lambda function to build a mask (boolean vector), and then apply the mask to Series. (http://pandas.pydata.org/pandas-docs/version/0.19/generated/pandas.Series.map.html)

import pandas as pd

str="Ah meta description the last bastion of traditional marketing! The only cross-over point between marketing and search engine optimisation! The knife edge between beautiful branding and an online suicide note!"
listStr = str.split(" ")
_s1 = pd.Series(listStr)
_s2 = _s1.map(lambda x: (x.count('a') + x.count('i') + x.count('o') + x.count('e') + x.count('u')) >= 3)
df = pd.concat([_s1,_s2],axis=1)
bl = df[1]==True
print((df[bl][0]).to_string(index=False))